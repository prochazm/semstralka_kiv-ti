package cz.zcu.fav.kiv.ti.huffman.cli;

import cz.zcu.fav.kiv.ti.huffman.codec.IHuffmanTask;

import java.nio.file.FileAlreadyExistsException;

/**
 * ARunner abstract class
 *
 * This is generic implementation of Runnable
 * interface intended for executing tasks implementing
 * IHuffmanTask interface. As tasks work with input
 * and output files you need to extend this and implement
 * validateTargetName abstract method so you can have
 * some control over target file requested by user.
 *
 * @author Martin Procházka
 * @since 14.11.2018
 */
abstract class ARunner implements Runnable{
    /**
     * Task to be run
     */
    private final IHuffmanTask task;

    /**
     * True if user does not want progress bars
     */
    private boolean noProgress;

    /**
     * True if user does not mind overwriting files
     */
    private boolean force;

    /**
     * Specifies intensity of System.out usage
     */
    private int print;

    /**
     * Path to target file
     */
    String target;
    /**
     * Path to source file
     */
    final String source;

    /**
     * Implement this to validate target file path.
     * FileAlreadyExistsException is expected to be thrown
     * if file already exists.
     *
     * Validation is run before executing the task.
     *
     * @throws FileAlreadyExistsException if file exists
     */
    abstract void validateTargetName() throws FileAlreadyExistsException;

    /**
     * Pass true if do not want to see progress bars.
     * Take care here as the rendered ProgressBars do
     * not support concurrent rendering so you may
     * want to touch this in threaded environment.
     *
     * Default value is false.
     *
     * @param noProgress true for verbose output
     */
    public void setNoProgress(final boolean noProgress) {
        this.noProgress = noProgress;
    }

    /**
     * Pass 1 if you want to print info about task
     * after it is initialized. Or 2 to also print
     * structures used for conversion the task.
     *
     * @param print 1 to print statistics, 2 to print tables too
     */
    void setPrint(final int print){
        this.print = print;
    }

    /**
     * If true is passed, all filename collisions are solved
     * by overwriting the former file. Properly implemented
     * ARunner throws FileNotFoundException if false is passed
     * and collisions are present.
     *
     * Default value is false.
     *
     * @param force true for force-overwrite on collision
     */
    void setForce(final boolean force){
        this.force = force;
    }

    /**
     * Returns true if force-overwrite is enabled or false otherwise
     *
     * @return true if force-overwrite
     */
    boolean isForce(){
        return this.force;
    }

    /**
     * Class constructor takes reference to task it shall execute,
     * path to source file and path to target file.
     *
     * It also initializes verbosity and force-overwrite to false.
     *
     * @param task reference to task
     * @param source path to source file
     * @param target path to target file
     */
    ARunner(final IHuffmanTask task, final String source, final String target){
        this.noProgress = false;
        this.force = false;
        this.task = task;
        this.target = target;
        this.source = source;
    }

    /**
     * This is implementation of Runnable#run.
     *
     * Upon calling this validation of target name is performed and
     * task is executed either silently or verbosely depending on
     * configuration of this runner.
     */
    @Override
    public void run(){
        try {
            this.validateTargetName();
            if (this.print > 0){
                this.task.echoPre(this.print == 2);
            }
            if (this.noProgress) {
                this.task.doTask(this.target);
            }else {
                this.task.doTaskVerbosely(this.target);
            }
            if (this.print > 0){
                this.task.echoPost(this.print == 2);
            }
        } catch (final Exception e) {
            System.out.println(String.format("Thread execution failed: %s", e.getMessage()));
        }
    }
}
