package cz.zcu.fav.kiv.ti.huffman.cli;

/**
 * CodecAction enum
 * <br><br>
 * This enum is used to differentiate HuffmanTasks
 *
 * @since 13.11.2018
 * @author Martin Procházka
 */
enum CodecAction {

    Encode("encode"), Decode("decode");

    /**
     * String value of enum
     */
    private final String asString;

    /**
     * Constructor sets string value of the enum.
     *
     * @param asString value
     */
    CodecAction(final String asString){
        this.asString = asString;
    }

    /**
     * Returns Encode if true and Decode if false.
     *
     * @param isEncoding true if you want Encode
     * @return Encode if true otherwise Decode
     */
    static CodecAction encoding(final boolean isEncoding){
        return isEncoding ? Encode : Decode;
    }

    /**
     * Overridden toString() returns content of asString.
     *
     * @return string representation
     */
    @Override
    public String toString(){
        return this.asString;
    }
}
