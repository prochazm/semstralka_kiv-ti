package cz.zcu.fav.kiv.ti.huffman.cli;

import me.tongfei.progressbar.ProgressBar;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.BiConsumer;

/**
 * CodecExecutor
 * <br><br>
 * This class manages configuration and execution of
 * runners. It supports serialized execution and
 * parallel execution with upper limit of threads
 * defined in class constructor.
 *
 * @since 13.11.2018
 * @author Martin Procházka
 */
class CodecExecutor {

    /**
     * List of source files
     */
    private final List<String> sources;

    /**
     * List of target files
     */
    private final List<String> targets;

    /**
     * Action this Executor will execute
     */
    private final CodecAction action;

    /**
     * Upper limit of concurrent thread to be executed.
     */
    private final int jobs;

    /**
     * Force file overwrites on conflicts
     */
    private boolean forceRewrite;

    /**
     * Print pre and post messages of the Tasks
     */
    private int verbosity;

    /**
     * Class constructor sets up class variables.
     * @param sources source files
     * @param targets target files
     * @param jobs thread limit
     * @param action action supported by this executor
     */
    CodecExecutor(final List<String> sources, final List<String> targets, final int jobs, final CodecAction action){
        this.action = action;
        this.sources = sources;
        this.targets = targets;
        this.jobs = jobs < 1
                ? Integer.min(sources.size(), Runtime.getRuntime().availableProcessors())
                : jobs;
    }

    /**
     * If true is passed forceRewrite is enabled
     * and file collisions will be solved by overwriting
     * the colliding file.
     *
     * @param forceRewrite enable force overwriting
     * @return this instance of executor
     */
    CodecExecutor forceRewrite(final boolean forceRewrite){
        this.forceRewrite = forceRewrite;
        return this;
    }

    /**
     * Sets level of verbosity. 1 means pre and post messages
     * will be printed, 2 means data structures are printed too.
     *
     * @param verbosity level of verbosity
     * @return this instance of executor
     */
    CodecExecutor verbosity(final int verbosity){
        this.verbosity = verbosity;
        return this;
    }

    /**
     * Execute all tasks with current configuration.
     *
     * @throws InterruptedException if thread get interrupted
     */
    void exec() throws InterruptedException {
        final ThreadPoolExecutor executor = this.newExecutor();
        final boolean multicall = this.jobs > 1;
        final boolean noProgress = this.verbosity == 0;

        this.forEachPair((s, t) -> executor.submit(this.configureRunner(s, t, multicall || noProgress)));
        executor.shutdown();

        if (!noProgress && multicall) {
            try (final ProgressBar progressBar = new ProgressBar(
                    String.format("Running %s task", this.action.toString()),
                    executor.getTaskCount())) {

                while (executor.isTerminating()) {
                    progressBar.stepTo(executor.getCompletedTaskCount());
                    Thread.sleep(1000);
                }
                progressBar.stepTo(progressBar.getMax());
            }
        }
    }

    /**
     * Creates runner for passed source and target file
     * and configures it according to according to actual
     * configuration of this CodecExecutor instance.
     * If verbose is true then progress bars are shown.
     *
     * @param source source file
     * @param target target file
     * @param noProgress disable progress bars
     * @return configured runner
     */
    private ARunner configureRunner(final String source, final String target, final boolean noProgress){
        final ARunner runner = this.action == CodecAction.Encode
                ? EncodeRunner.getRunner(source, target)
                : DecodeRunner.getRunner(source, target);

        runner.setPrint(this.verbosity - 1);
        runner.setForce(this.forceRewrite);
        runner.setNoProgress(noProgress);

        return runner;
    }

    /**
     * Initializes thread executor for actual number of jobs.
     *
     * @return instance of ThreadPoolExecutor
     */
    private ThreadPoolExecutor newExecutor(){
        return (ThreadPoolExecutor) Executors.newFixedThreadPool(this.jobs);
    }

    /**
     * Items in this foreach are source file and target file
     * with matching index in their collections. If there is
     * no target file for the source file then targets name
     * is assigned to be same according source file.
     *
     * @param biConsumer biConsumer
     */
    private void forEachPair(final BiConsumer<String, String> biConsumer){
        for (int i = 0; i < this.sources.size(); ++i){
            final String source = this.sources.get(i);
            String target = i >= this.targets.size()
                    ? this.sources.get(i)
                    : this.targets.get(i);
            if (target.equals("")){
                target = this.sources.get(i);
            }
            biConsumer.accept(source, target);
        }
    }

    /**
     * Overridden toString returns status of this CodecExecutor.
     *
     * @return status
     */
    @Override
    public String toString(){
        return String.format("Prepared %d tasks and %d jobs...",
                this.sources.size(), this.jobs);
    }

}
