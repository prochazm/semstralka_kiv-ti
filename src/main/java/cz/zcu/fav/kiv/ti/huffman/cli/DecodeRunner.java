package cz.zcu.fav.kiv.ti.huffman.cli;

import cz.zcu.fav.kiv.ti.huffman.codec.Decoder;
import cz.zcu.fav.kiv.ti.huffman.codec.IHuffmanTask;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * DecodeRunner class
 * <br><br>
 * This class is extension to abstract ARunner
 * and defines its behavior for Decode task.
 *
 * @see ARunner
 * @since 13.11.2018
 * @author Martin Procházka
 */
class DecodeRunner extends ARunner {

    /**
     * Static factory method returns instance of this runner
     * for specified source and target file defined by file
     * path.
     *
     * @param source path to source file
     * @param target path to target file
     * @return reference to new DecodeRunner instance
     */
    static DecodeRunner getRunner(final String source, final String target) {
        return new DecodeRunner(Decoder.getDecoder(source), source, target);
    }

    /**
     * Validates target file.
     * <br><br>
     * If path to source file equals path to target file then
     * target file is stripped of its ending. If force-rewrite
     * is disabled and file on target's path exists then
     * FileAlreadyExistsException is thrown.
     *
     * @throws FileAlreadyExistsException in case of name collision and force disabled
     */
    void validateTargetName() throws FileAlreadyExistsException {
        if (super.source.equals(super.target) && super.target.lastIndexOf(".") > 0) {
            super.target = super.target.substring(0, super.target.lastIndexOf("."));
        }
        if (!super.isForce() && Files.exists(Paths.get(super.target))) {
            throw new FileAlreadyExistsException(String.format("File already exists: %s", super.target));
        }
    }

    /**
     * Class constructor
     * <br><br>
     * Only passes arguments to superclass.
     * @param decoder decoder typed as IHuffmanTask
     * @param source path to source file
     * @param target path to target file
     */
    private DecodeRunner(final IHuffmanTask decoder, final String source, final String target) {
        super(decoder, source, target);
    }

}
