package cz.zcu.fav.kiv.ti.huffman.cli;

import cz.zcu.fav.kiv.ti.huffman.codec.Encoder;
import cz.zcu.fav.kiv.ti.huffman.codec.IHuffmanTask;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * EncodeRunner class
 * <br><br>
 * This class is extension to abstract ARunner
 * and defines its behavior for Encode task.
 *
 * @see ARunner
 * @since 14.11.2018
 * @author Martin Procházka
 */
class EncodeRunner extends ARunner {
    /**
     * File path mask
     */
    private static final String TARGET_TEMPLATE = "%s.huf";

    /**
     * Static factory method returns instance of this runner
     * for specified source and target file defined by file
     * path.
     *
     * @param source path to source file
     * @param target path to target file
     * @return reference to new EncodeRunner instance
     */
    static EncodeRunner getRunner(final String source, final String target) {
        return new EncodeRunner(Encoder.getEncoder(source), source, target);
    }

    /**
     * Validated target file.
     * <br><br>
     * Applies mask to target file, if resulting file already exists
     * and force-overwrite is enabled exception is thrown.
     *
     * @throws FileAlreadyExistsException if name collision occurs
     */
    void validateTargetName() throws FileAlreadyExistsException {
        super.target = String.format(TARGET_TEMPLATE, super.target);
        if (!super.isForce() && Files.exists(Paths.get(super.target))) {
            throw new FileAlreadyExistsException(String.format("File already exists: %s", super.target));
        }
    }

    /**
     * Class constructor
     * <br><br>
     * Only passes arguments to superclass
     * @param encoder encoder typed as IHuffmanTask
     * @param source path to source file
     * @param target path to target file
     */
    private EncodeRunner(final IHuffmanTask encoder, final String source, final String target){
        super(encoder, source, target);
    }
}
