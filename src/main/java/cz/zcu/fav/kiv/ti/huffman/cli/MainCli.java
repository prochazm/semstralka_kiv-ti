package cz.zcu.fav.kiv.ti.huffman.cli;


import picocli.CommandLine;

import java.util.List;
import java.util.concurrent.Callable;

import static picocli.CommandLine.*;

/**
 * MainCli class
 * <br><br>
 * This the main class.
 *
 * @since 12.11.2018
 * @author Martin Procházka
 */
@Command(name = "huffman", footer = "Copyright(c) 2018 Martin Procházka, under GNU GPLv3",
            description = "Encode or decode a file with huffman tree, huffman trees are awesome!")
class MainCli implements Callable<Boolean> {
    /**
     * Parsed list of source files
     */
    @Parameters(index = "0", split = ",", defaultValue = "",
            description = "Source file separated by comma.")
    private List<String> sources;

    /**
     * Parsed list of target files
     */
    @Parameters(index = "1", split = ",", defaultValue = "",
            description = "Target files separated by comma. These map to source files one on one if there is not " +
                    "enough target files listed then the missing ones are substituted by source file with suffix " +
                    "added or removed depending on mode")
    private List<String> targets;

    /**
     * Parsed force flag
     */
    @Option(names = {"-f", "--force"},
            description = "With this flag present any file name collision that occurs will be solved destructively.")
    private boolean force;

    /**
     * Parsed encode flag
     */
    @Option(names = {"-e", "--encode"},
            description = "Use this flag to enable encoder mode. " +
                    "This can't be used with -d flag and can be omitted as it's the default value.")
    private boolean compress;

    /**
     * Parsed decode flag
     */
    @Option(names = {"-d", "--decode"},
            description = "Use this flag to enable decoder mode. This can't be used with -e flag.")
    private boolean decompress;

    /**
     * Parsed verbose multi-flag
     */
    @Option(names = {"-v", "--verbose"},
            description = "Show progress bars and more, there are 3 levels of verbosity.")
    private boolean verbose[] = new boolean[0];

    /**
     * Parsed help flag
     */
    @Option(names = {"-h", "--help"}, usageHelp = true,
            description = "Show this message.")
    private boolean helpRequested;

    /**
     * Parsed count of jobs
     */
    @Option(names = {"-j", "--jobs"}, defaultValue = "1",
            description = "Specify how many jobs shall run at once. Default is 1, parallelism is applied at the " +
                    "level of tasks so changing this only makes sense with multiple source files to encode.")
    private int jobs;

    /**
     * This is entry point of cli. Command line
     * arguments are parsed and call() called.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        if (args.length == 0){
            args = new String[]{"--help"};
        }
        CommandLine.call(new MainCli(), args);
    }

    /**
     * Configures CodecExecutor according to the contract with user
     * and initiates execution of all tasks.
     *
     * @return True if command line arguments are valid
     * @throws Exception mainly IO exceptions from accessing source/target files
     */
    @Override
    public Boolean call() throws Exception {

        if (this.compress && this.decompress || this.targets.isEmpty()) {
            return false;
        }
        if (!this.decompress) {
            this.compress = true;
        }

        new CodecExecutor(this.sources, this.targets, this.jobs, CodecAction.encoding(this.compress))
                .forceRewrite(this.force)
                .verbosity(this.verbose.length)
                .exec();

        return true;
    }
}
