package cz.zcu.fav.kiv.ti.huffman.codec;

import cz.zcu.fav.kiv.ti.huffman.util.Statistics;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Analyser class
 * <br><br>
 * This class is used by Encoder to analyse input
 * filePath and prepare data structures needed for
 * encoding it.
 *
 * @since 7.11.2018
 * @author Martin Procházka
 */
class Analyser {
    /**
     * Path to analysed filePath
     */
    private final Path filePath;

    /**
     * Map storing character occurrences in analysed filePath
     */
    private final Statistics occurrences;

    /**
     * Huffman tree built from occurrences
     */
    private HuffmanTree huffmanTree;

    /**
     * Compressed filePath of the filePath
     */
    private long compBitSize;

    /**
     * Uncompressed size of the filePath
     */
    private long rawByteSize;

    /**
     * Calculated as Compressed size / raw size
     */
    private double compressionRatio;

    /**
     * Class constructor sets input filePath.
     *
     * @param file path to input filePath
     */
    Analyser(final String file){
        this.occurrences = new Statistics();
        this.filePath = Paths.get(file);
    }

    /**
     * Returns occurrence map.
     *
     * @return occurrence map
     * @throws IOException if IO fails
     */
    private Statistics getOccurrences() throws IOException {
        if (this.occurrences.size() != 0){
            return this.occurrences;
        }
        try(final BufferedInputStream is = new BufferedInputStream(Files.newInputStream(this.filePath))){
            return this.getOccurrences(is);
        }
    }

    /**
     * Returns occurrence map in verbose manner.
     *
     * @param verbose true to see progressbar
     * @return occurrence map
     * @throws IOException if IO fails
     */
    Statistics getOccurrences(final boolean verbose) throws IOException {
        if (this.occurrences.size() != 0){
            return this.occurrences;
        }
        if (verbose){
            try(final BufferedInputStream is = new BufferedInputStream(ProgressBar.wrap(
                    Files.newInputStream(this.filePath),
                    new ProgressBarBuilder()
                            .setUnit(" B", 1)
                            .setTaskName("Mapping  " + this.getFilePath())
                            .setInitialMax(this.getRawSize()))))
            {
                return this.getOccurrences(is);
            }
        }
        return this.getOccurrences();
    }

    /**
     * Return occurrence map for given BufferedInputStream.
     *
     * @param is input stream
     * @return occurrence map
     * @throws IOException if IO fails
     */
    private Statistics getOccurrences(final BufferedInputStream is) throws IOException {
        for (int c = is.read(); c != -1; c = is.read()) {
            this.occurrences.increment(c);
        }

        if (this.occurrences.size() == 0){
            throw new IOException("File is empty, nothing to analyse.");
        }

        return this.occurrences;
    }

    /**
     * Returns reference to instance of HuffmanTree for this
     * input filePath.
     *
     * @return HuffmanTree
     * @throws IOException is IO fails
     * @throws BrokenDataSetException if tree fails to build
     */
    HuffmanTree getHuffmanTree() throws IOException, BrokenDataSetException {
        return this.huffmanTree == null
                ? (this.huffmanTree = new HuffmanTree(this.getOccurrences(false)))
                :  this.huffmanTree;

    }

    /**
     * Returns path to input filePath
     *
     * @return input filePath path
     */
    Path getFilePath(){
        return this.filePath;
    }

    /**
     * Return calculated compressed size of input filePath in bits.
     *
     * @return compressed size in bits
     * @throws IOException is IO fails
     * @throws BrokenDataSetException if tree fails to build
     */
    long getCompressedSize() throws IOException, BrokenDataSetException {
        if (this.compBitSize > 0){
            return this.compBitSize;
        }

        final HuffmanTree tree = this.getHuffmanTree();
        this.compBitSize = 0;

        for (final Statistics.Occurrence o : this.occurrences) {
            this.compBitSize += o.getValue() * tree.getItem(o.getKey()).getLength();
        }
        this.compBitSize += tree.getHeader().length * Byte.SIZE;
        this.compBitSize -= tree.getHeaderOffset();

        return this.compBitSize;
    }

    /**
     * Returns raw size of the filePath in bytes.
     *
     * @return raw size
     */
    long getRawSize() throws IOException {
        return (this.rawByteSize == 0)
                ? this.rawByteSize = Files.size(this.filePath)
                : this.rawByteSize;
    }

    /**
     * Returns compression ratio.
     *
     * @return compression ratio
     * @throws IOException is IO fails
     * @throws BrokenDataSetException if tree fails to build
     */
    double getCompressionRatio() throws IOException, BrokenDataSetException {
        return this.compressionRatio == 0
                ? (this.compressionRatio = (double)this.getRawSize()) / (this.getCompressedSize() >> 3)
                :  this.compressionRatio;
    }

    /**
     * Returns count of trailing bits at the end of the
     * compressed input filePath.
     *
     * @return count of trailing bits
     * @throws IOException if IO fails
     * @throws BrokenDataSetException if tree fails to build
     */
    byte getTrailingBitsCount() throws IOException, BrokenDataSetException {
        return (byte)((8 - this.getCompressedSize()) & 7);
    }
}
