package cz.zcu.fav.kiv.ti.huffman.codec;

/**
 * BrokenDataSetException class
 * <br><br>
 * This is extension to Exception.
 *
 * @since 12.11.2018
 * @author Martin Procházka
 */
class BrokenDataSetException extends Exception {

    /**
     * Calls super with predefined message.
     */
    BrokenDataSetException() {
        super("Data set is broken. Pray and try again.");
    }

    BrokenDataSetException(final String s) {super(s);}
}
