package cz.zcu.fav.kiv.ti.huffman.codec;

import java.util.Objects;

/**
 * Code class
 * <br><br>
 * Messenger used to transfer the huffman code
 * and its length.
 *
 * @since 17.11.2018
 * @author Martin Procházka
 */
class Code {
    /**
     * Actual code aligned to right
     */
    private final int code;

    /**
     * Length of the code
     */
    private final int length;

    /**
     * Class constructor sets code and its length.
     *
     * @param code huffman code
     * @param length length of the code
     */
    Code(final int code, final int length){
        this.code = code;
        this.length = length;
    }

    /**
     * Returns the code
     *
     * @return code
     */
    int getCode() {
        return this.code;
    }

    /**
     * Returns length of the code
     *
     * @return code length
     */
    int getLength() {
        return this.length;
    }

    /**
     * Overridden equals returns true if code
     * and length matches.
     *
     * @param o object to compare
     * @return true if code and length is the same
     */
    @Override
    public boolean equals(final Object o) {
        if (o instanceof Code){
            final Code c = (Code)o;
            return this.code == c.getCode() && this.length == c.getLength();
        }
        return false;
    }

    /**
     * Returns hash based on code and its length
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.getCode(), this.getLength());
    }

}
