package cz.zcu.fav.kiv.ti.huffman.codec;

import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Decoder class
 * <br><br>
 * This class is implementation of IHuffmanTask
 * interface. As name suggests its purpose is to
 * decode a file encoded by Encoder class.
 *
 * @author Martin Procházka
 * @since 6.11.2018
 */
public class Decoder implements IHuffmanTask{

    /**
     * This is index where it still makes sense to write to buffer
     */
    private static final int BUFFER_WRITABLE_THRESHOLD = 3 * Integer.SIZE / 4;

    /**
     * Size of leaf indicator in parsed header
     */
    private static final int KEY_SIZE = 1;

    /**
     * Size of character in leaf
     */
    private static final int VALUE_SIZE = 8;

    /**
     * Size of leaf with its character in header
     */
    private static final int KEY_VALUE_SIZE = VALUE_SIZE + KEY_SIZE;

    /**
     * Value of non-leaf in compressed tree
     */
    private static final int NOT_LEAF = 0;

    /**
     * Size of buffer
     */
    private static final int BUFFER_SIZE = Integer.SIZE;

    /**
     * Count of bits used to represent count of trailing bit at the end of the file
     */
    private static final int TRAILING_BITS_RESOLUTION = 3;

    /**
     * Index in buffer, initial is 32, filled buffer has index 0
     */
    private int index = 32;

    /**
     * Buffer
     */
    private int buffer = 0;

    /**
     * Path to input file
     */
    private final Path file;

    /**
     * Path to last target file
     */
    private Path lastTarget;

    /**
     * Reference to huffmanTree used to decode the file.
     */
    private HuffmanTree huffmanTree;

    /**
     * Private constructor only sets input file.
     *
     * @param file input filepath
     */
    private Decoder(final String file){
        this.file = Paths.get(file);
    }

    /**
     * Static factory method returns reference to new
     * decoder instance with its source file set to
     * the one on path passed as argument.
     *
     * @param file input filepath
     * @return reference to decoder instance.
     */
    public static IHuffmanTask getDecoder(final String file){
        return new Decoder(file);
    }

    /**
     * Calls decode with buffered streams, input stream is
     * wrapped by progressbar. Target is path to output file.
     *
     * @param target output file
     * @throws Exception if IO fails
     */
    @Override
    public void doTaskVerbosely(final String target) throws Exception {
        this.lastTarget = Paths.get(target);
        try(final BufferedOutputStream os = new BufferedOutputStream(Files.newOutputStream(this.lastTarget));
            final BufferedInputStream is = new BufferedInputStream(ProgressBar.wrap(
                    Files.newInputStream(this.file),
                    new ProgressBarBuilder()
                            .setUnit(" B", 1)
                            .setTaskName("Decode " + this.file)
                            .setInitialMax(Files.size(this.file)))))
        {
            this.decode(is, os);
        }
    }

    /**
     * Calls decode with buffered streams. Target is
     * file destination on filesystem for decoded data.
     *
     * @param target output file
     * @throws Exception if IO fails
     */
    @Override
    public void doTask(final String target) throws Exception {
        this.lastTarget = Paths.get(target);
        try(final BufferedOutputStream os = new BufferedOutputStream(Files.newOutputStream(Paths.get(target)));
            final BufferedInputStream is = new BufferedInputStream(Files.newInputStream(this.file))){
            this.decode(is, os);
        }
    }

    /**
     * Takes input and output stream as arguments and tries
     * to decode data from the input stream. The input stream
     * has to start with proper header otherwise it won't be
     * possible to encode anything. Decoded characters are
     * written to the output stream.
     *
     * @param is input stream
     * @param os output stream
     * @throws Exception when IO fails
     */
    private void decode(final BufferedInputStream is, final BufferedOutputStream os) throws Exception {
        final HuffmanTreeTopDownBuilder treeBuilder = new HuffmanTreeTopDownBuilder();

        this.fillBuffer(is);

        final byte trailingBits = (byte) (this.buffer >>> BUFFER_SIZE - 3);
        this.buffer <<= TRAILING_BITS_RESOLUTION;
        this.index += TRAILING_BITS_RESOLUTION;

        final int endOfFileThreshold = BUFFER_SIZE - trailingBits;
        final int leafReadableThreshold = BUFFER_SIZE - KEY_VALUE_SIZE;

        //load tree
        while (!treeBuilder.isComplete()) {
            final int tmp = this.buffer >>> BUFFER_SIZE - KEY_SIZE;

            if (tmp == NOT_LEAF) {
                treeBuilder.addNode(-1);
                this.buffer <<= KEY_SIZE;
                ++this.index;
            } else {
                treeBuilder.addNode((this.buffer << KEY_SIZE) >>> BUFFER_WRITABLE_THRESHOLD);
                this.buffer <<= KEY_VALUE_SIZE;
                this.index += KEY_VALUE_SIZE;
            }
            if (this.index > leafReadableThreshold) {
                if (!this.fillBuffer(is) && this.index >= endOfFileThreshold) {
                    throw new Exception("Could not build a tree, stream ended too soon.");
                }
            }
        }

        this.huffmanTree = treeBuilder.toProperTree();
        //todo implement skipping tree building if it's build from last run, low priority.

        // you don't want to use number higher than eofThreshold as it could make the while condition false
        // even though input stream is not fully exhausted.
        final int safeTraversalThreshold = Integer.min(
                BUFFER_SIZE - this.huffmanTree.getMaxDepth(), endOfFileThreshold
        );

        while (this.index < endOfFileThreshold) {
            //consume buffer
            final Code result = this.huffmanTree.traverseUntil(this.buffer >>> this.index, BUFFER_SIZE - this.index);

            this.buffer <<= result.getLength();
            this.index += result.getLength();
            os.write(result.getCode());
            if (this.index >= safeTraversalThreshold){
                this.fillBuffer(is);
            }
        }
    }

    /**
     * Fill buffer and return false if stream has ended or
     * true otherwise.
     *
     * @param is stream
     * @return false if stream ended
     * @throws IOException thrown if stream cannot be read.
     */
    private boolean fillBuffer(final InputStream is) throws IOException {
        int b = 0;

        while (this.index >= Byte.SIZE && (b = is.read()) != -1){
            this.index -= Byte.SIZE;
            this.buffer |= b <<= this.index;
        }

        return b != -1;
    }

    /**
     * Prints that encoder is initialized. Argument is not used.
     *
     * @param more not used.
     */
    @Override
    public void echoPre(final boolean more) {
        System.out.println(String.format("Decoder initialized for file %s ...", this.file));
    }

    /**
     * Prints decompressed and compressed sizes of the processed
     * file and if more is true code table is shown too.
     *
     * @param more show code table
     */
    @Override
    public void echoPost(final boolean more) throws IOException {
        final long sourceSize = Files.size(this.file);
        final long targetSize = Files.size(this.lastTarget);

        System.out.printf("File %s decompressed\n", this.file);
        System.out.printf("File raw size:\t\t%10d [B]\n", sourceSize);
        System.out.printf("File decoded size:\t%10d [B]\n", targetSize);

        if (more){
            final Code[] codeMap = this.huffmanTree.getItems();
            final StringBuilder sb = new StringBuilder("\nCode map: char[hex]:code\n");
            int x = 0;
            for (int i = 0; i < codeMap.length; ++i) {
                final Code c;
                if ((c = codeMap[i]) == null){
                    continue;
                }
                sb.append(String.format("%c[%X]:%-12s\t",
                        (i < 32 || i < 160 && i > 127) ? ' ' : i,
                        i, Integer.toBinaryString(1 << c.getLength() | c.getCode()).substring(1)));

                if (++x == 6){
                    x = 0;
                    sb.append("\n");
                }
            }
            System.out.println(sb.toString());
            System.out.printf("Closing decoder for %s ...\n", this.file);
        }
    }
}
