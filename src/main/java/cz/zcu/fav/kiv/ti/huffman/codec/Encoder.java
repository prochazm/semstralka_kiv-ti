package cz.zcu.fav.kiv.ti.huffman.codec;

import cz.zcu.fav.kiv.ti.huffman.util.Statistics;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Encoder class
 *
 * It's sole purpose is to encode bytes from input to Huffman
 * encoded sequence. HuffmanTree which this instance of
 * encoder will use is specified in its constructor.
 *
 * @author Martin Procházka
 * @since 23.10.2018
 */
public class Encoder implements IHuffmanTask{

    /**
     * Reference to input analyser
     */
    private final Analyser analyser;

    /**
     * Private constructor takes reference to Analyser
     * instance. Use {@link Encoder#getEncoder(String)} to create new encoders
     * from outer scope.
     *
     * @param analyser reference to analyser
     */
    private Encoder(final Analyser analyser) {
        this.analyser = analyser;
    }

    /**
     * This static factory method creates new Encoder
     * instance and returns reference to it typed to IHuffmanTask
     * interface. The returned encoder is to be used exclusively
     * for the file on path passed as argument.
     *
     * @param file path to file to be encoded
     * @return reference to encoder
     */
    public static IHuffmanTask getEncoder(final String file) {
        return new Encoder(new Analyser(file));
    }

    /**
     * This is implementation of {@link IHuffmanTask#doTaskVerbosely(String)}.
     * <br><br>
     * Upon calling this the source file of this encoder will
     * be encoded in verbose manner with target location specified
     * by passed argument.
     * <br><br>
     * Exceptions related to IO might be thrown.
     *
     * @param target target location
     * @throws Exception if IO goes wrong
     */
    @Override
    public void doTaskVerbosely(final String target) throws Exception {
        this.analyser.getOccurrences(true); //let's do this verbosely
        try (final BufferedOutputStream os = new BufferedOutputStream(Files.newOutputStream(Paths.get(target)));
             final BufferedInputStream is = new BufferedInputStream(
                     ProgressBar.wrap(
                             Files.newInputStream(this.analyser.getFilePath()),
                             new ProgressBarBuilder()
                                 .setUnit(" B", 1)
                                 .setTaskName("Encoding " + this.analyser.getFilePath())
                                 .setInitialMax(this.analyser.getRawSize()))))
        {
            this.encode(is, os);
        }
    }

    /**
     * This is implementation of {@link IHuffmanTask#doTask(String)}.
     * <br><br>
     * Upon calling this the source file of this encoder will
     * be encoded in dead silent manner with target location specified
     * by passed argument.
     * <br><br>
     * Exceptions related to IO might be thrown.
     *
     * @param target target location
     * @throws Exception if IO goes wrong
     */
    @Override
    public void doTask(final String target) throws Exception {
        try (final BufferedOutputStream os = new BufferedOutputStream(Files.newOutputStream(Paths.get(target)));
             final BufferedInputStream is = new BufferedInputStream(Files.newInputStream(this.analyser.getFilePath()))){
            this.encode(is, os);
        }
    }

    /**
     * This method outputs some basic information about the file
     * and its compression. If more is true occurrence map is also
     * outputted.
     *
     * @param more outputs occurrence map if true
     */
    @Override
    public void echoPre(final boolean more) {
        try {
                this.analyser.getOccurrences(true);
                System.out.println(String.format("Encoder initialized for %s ...", this.analyser.getFilePath()));
                System.out.println(String.format("File raw size:\t\t%10d [B]", this.analyser.getRawSize()));
                System.out.println(String.format("File encoded size:\t%10d [B]",
                        this.analyser.getCompressedSize() + 0b111 >> 3));
                System.out.println(String.format("Header size:\t\t%10d [B] %10d [b]",
                        this.analyser.getHuffmanTree().getHeader().length,
                        this.analyser.getHuffmanTree().getHeader().length * Byte.SIZE - this.analyser.getHuffmanTree().getHeaderOffset()));
                System.out.println(String.format("Trailing bits:\t\t%14s %10d [b]", "", this.analyser.getTrailingBitsCount()));
                System.out.println(String.format("Compression ratio:\t%14.3f", this.analyser.getCompressionRatio()));

                if (more){
                    final Statistics occurrences = this.analyser.getOccurrences(true);
                    final StringBuilder sb = new StringBuilder("Occurrence map: char[hex value]:count of occurrences\n");
                    int i = 0;
                    for (final Statistics.Occurrence o : occurrences) {
                        final int key = o.getKey();
                        sb.append(String.format("%c[%02X]:%-6d\t",
                                (key < 32 || key < 160 && key > 127) ? ' ' : key,
                                key, o.getValue()));

                        if (++i == 6){
                            i = 0;
                            sb.append("\n");
                        }
                    }
                    System.out.println(sb.toString());
                }
        } catch (final IOException | BrokenDataSetException e) {
            System.out.println(String.format("Error occurred while parsing statistics: %s\n", e.getMessage()));
        }
    }

    @Override
    public void echoPost(final boolean more){
        System.out.println(String.format("Closing encoder for %s ... ", this.analyser.getFilePath()));
    }

    /**
     * When I wrote this God and I knew how it works. Now only
     * God knows.
     * <br><br>
     * Input is taken from passed BufferedInputsStream instance
     * then encoded byte by byte and vomited into passed instance
     * of BufferedOutputStream.
     *
     * @param is input stream
     * @param os output stream
     * @throws Exception if IO goes wrong
     */
    private void encode(final BufferedInputStream is, final BufferedOutputStream os) throws Exception {
        final HuffmanTree huffmanTree = this.analyser.getHuffmanTree();
        final byte[] header = huffmanTree.getHeader();

        if (header == null){
            return; // empty file maps to empty file
        }

        // prepend header with count of trailing bits
        os.write(header[0] | this.analyser.getTrailingBitsCount() << Byte.SIZE - 3);
        os.write(header, 1, header.length - 2);
        int buffer = Byte.toUnsignedInt(header[header.length - 1]) >>> huffmanTree.getHeaderOffset();
        int k = Byte.SIZE - huffmanTree.getHeaderOffset();


        int character;
        while ((character = is.read()) != -1) {
            final Code code = huffmanTree.getItem(character);
            int codeVal = code.getCode();
            int codeLen = code.getLength();
            int space;

            // if code doesn't fit to the buffer
            while (codeLen > (space = Byte.SIZE - k)){
                k = 0;
                if (space == 0) {
                    os.write(buffer);
                    continue;
                }
                // fill the buffer a flush it
                codeLen -= space;
                buffer <<= space;
                buffer |=  codeVal >>> codeLen;
                os.write(buffer);
                // adjust the code and its size
                codeVal <<= Integer.SIZE - codeLen;
                codeVal >>>= Integer.SIZE - codeLen;
            }
            // now code surely fits the buffer, push it
            buffer <<= codeLen;
            buffer |= codeVal;
            k += codeLen;
        }

        buffer <<= Byte.SIZE - k;
        os.write(buffer);
    }
}
