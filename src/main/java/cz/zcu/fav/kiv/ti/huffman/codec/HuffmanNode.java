package cz.zcu.fav.kiv.ti.huffman.codec;

/**
 * HuffmanNode class
 *
 * Each instance of this class should represent
 * single node in Huffman tree. It's safe to assume
 * that every Node has either two or none children.
 * In case it has none we call it leaf.
 * <br><br>
 * Also each instance is meant to be immutable. You can
 * specify the fact that it is leaf by using appropriate
 * constructor.
 * <br><br>
 * This class implements Comparable&lt;HuffmanNode&gt;
 * interface. Comparison of HuffmanNodes is based solely
 * on count of occurrences, see {@link #getCount()},
 * {@link #compareTo(HuffmanNode)}.
 *
 * @author Martin Procházka
 * @since 23.10.2018
 */
class HuffmanNode implements Comparable<HuffmanNode> {
    /**
     * Count of occurrences inside the input file.
     */
    private final int count;

    /**
     * Integer value of stored item (which is probably character)
     */
    private final int item;

    /**
     * Reference to left child
     */
    private final HuffmanNode leftChild;

    /**
     * Reference to right child
     */
    private final HuffmanNode rightChild;

    /**
     * True if this node is leaf otherwise false
     */
    private final boolean isLeaf;

    /**
     * Class constructor, takes value and its occurrence.
     * Use this constructor only for leaves, for non-leaves
     * use the other constructor.
     *
     * @param value value to be encoded
     * @param occurrence occurrence of the value
     */
    HuffmanNode(final int value, final int occurrence){
        this.leftChild = this.rightChild = null;
        this.item = value;
        this.count = occurrence;
        this.isLeaf = true;
    }

    /**
     * Class constructor, takes left and right child. When using
     * this constructor the count property is set to sum of both
     * children count and item is set to -1. Use this constructor
     * only for non-leaves, for leaves use the other constructor.
     *
     * @param leftChild left child of this node
     * @param rightChild right child of this node
     */
    HuffmanNode(final HuffmanNode leftChild, final HuffmanNode rightChild){
        this.count = leftChild.getCount() + rightChild.getCount();
        this.leftChild = leftChild;
        this.rightChild = rightChild;
        this.item = -1;
        this.isLeaf = false;
    }

    /**
     * Return true if node is leaf otherwise false.
     *
     * @return true if node is leaf
     */
    boolean isLeaf(){
        return this.isLeaf;
    }

    /**
     * Returns reference to left child of this node.
     *
     * @return left child
     */
    HuffmanNode getLeftChild() {
        return this.leftChild;
    }

    /**
     * Return reference to right child of this node.
     *
     * @return right child
     */
    HuffmanNode getRightChild() {
        return this.rightChild;
    }

    /**
     * Returns occurrence count of this node's value.
     *
     * @return occurrence
     */
    int getCount(){ return this.count; }

    /**
     * Returns value which this node represents (character
     * from encoded alphabet)
     *
     * @return item
     */
    int getItem(){ return this.item; }

    /**
     * Returns occurrence count of this instance minus occurrence
     * count of instance passed as an argument.
     *
     * @param huffmanNode compared instance
     * @return int value
     */
    @Override
    public int compareTo(final HuffmanNode huffmanNode) {
        return this.count - huffmanNode.getCount();
    }
}
