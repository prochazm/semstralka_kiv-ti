package cz.zcu.fav.kiv.ti.huffman.codec;

import cz.zcu.fav.kiv.ti.huffman.util.Statistics;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * HuffmanTree class
 * <br><br>
 * This class is supposed to represent a Huffman tree
 * which is build of HuffmanNode instances and it exposes
 * some functionality to handle operations useful on
 * the tree like retrieving codes of encoded characters.
 *
 * @author Martin Procházka
 * @since 23.10.2018
 */
class HuffmanTree {

    /**
     * Cached tree, key is the character and value is its code
     */
    private Code[] huffmanCache;

    /**
     * Head of this tree
     */
    private HuffmanNode head;

    /**
     * Maximal depth of this tree
     */
    private int maxDepth;

    /**
     * File header for this tree
     */
    private byte[] header;

    /**
     * Count of trailing bits at the end of header
     */
    private int headerOffset;

    /**
     * Class constructor takes map of occurrences where key
     * is int value representing character and the value
     * for the key is int value representing count of
     * character occurrences in analyzed data. Also the inner
     * data structure of the tree is built when calling this
     * constructor.
     *
     * @param occurrences character occurrences
     * @throws BrokenDataSetException if tree could not be built
     */
    HuffmanTree(final Statistics occurrences) throws BrokenDataSetException {
        this.build(occurrences);
        this.buildCache();
        this.buildHeader();
    }

    /**
     * Use this constructor if you already have a Huffman
     * tree represented by its root node. (e.g. if you
     * managed to reconstruct tree from encoded stream)
     * <br><br>
     * If passed node is leaf then new root node will be
     * created and passed node will be its left and right
     * child. This is so the code of character represented
     * by such node would not be empty. Consequence of this
     * is that you expect the character to be encoded as
     * 0x01 with length of 1.
     *
     * @param head head of the tree
     */
    HuffmanTree(final HuffmanNode head){
        this.head = head.isLeaf() ? new HuffmanNode(head, head) : head;
        this.buildCache();
        this.buildHeader();
    }

    /**
     * Returns instance of HuffmanNode which is also
     * head of this tree.
     *
     * @return head
     */
    HuffmanNode getHead() {
        return this.head;
    }

    /**
     * Returns compressed version of this tree as byte
     * array. This is mainly used as header for encoded
     * file so decoder can reconstruct the tree from it.
     * The tree is compressed the way that if you read
     * 0-bit then node IS NOT a leaf, if you read 1-bit
     * then node IS a leaf and next BYTE.size bits
     * represent value of encoded character. Nodes are
     * ordered in PREORDER manner (NLR).
     *
     * @return compressed tree as byte array
     */
    byte[] getHeader(){
        return this.header;
    }

    /**
     * Returns count of trailing bits in the header of
     * this tree. Expect values between 0 and 7 inclusive.
     *
     * @return count of trailing header bit
     */
    int getHeaderOffset(){
        return this.headerOffset;
    }

    /**
     * Returns Huffman code for value passed as an argument.
     * This may end up raising null pointer is no such item
     * is present in the tree so you might want to call
     * hasItem first if you are not sure about its presence.
     *
     * @param item value to be encoded
     * @return Huffman code
     */
    Code getItem(final int item){
        return this.huffmanCache[item];
    }

    /**
     * Returns map containing all items as keys with their
     * codes as values.
     *
     * @return map of items and keys
     */
    Code[] getItems(){
        return this.huffmanCache;
    }

    /**
     * Returns True if such item exists in this tree
     * otherwise returns false.
     *
     * @param item item to be found
     * @return True if it is found
     */
    boolean hasItem(final int item){
        return this.huffmanCache[item] != null;
    }

    /**
     * Builds Huffman tree from occurrence map.
     *
     * @param occurrences occurrences
     * @throws BrokenDataSetException if data is broken
     */
    private void build(final Statistics occurrences) throws BrokenDataSetException {
        final PriorityQueue<HuffmanNode> heads = new PriorityQueue<>();
        for (final Statistics.Occurrence o : occurrences) {
            heads.offer(new HuffmanNode(o.getKey(), o.getValue()));
        }

        while (heads.size() > 1){
            final HuffmanNode leftChild = heads.poll();
            final HuffmanNode rightChild = heads.poll();

            if (leftChild == null || rightChild == null){
                throw new BrokenDataSetException();
            }

            final HuffmanNode parent = new HuffmanNode(leftChild, rightChild);
            heads.offer(parent);
        }

        this.head = heads.poll();
    }

    /**
     * This method call recursive buildCache.
     */
    private void buildCache(){
        this.huffmanCache = new Code[256];
        if (this.head != null){
            if (this.head.isLeaf()){
                this.huffmanCache[this.head.getItem()] = new Code(0x01, 1);
                return;
            }
            this.buildCache(this.head, 0, 0);
        }
    }

    /**
     * This method prepares internal cache of this tree.
     * Cached items are instances of Code {@link HuffmanTree#getItem(int)}
     * <br><br>
     * Call this without arguments to start building the cache
     * from the head down.
     *
     * @param node node
     * @param code code of the node
     * @param depth length of the code
     */
    private void buildCache(final HuffmanNode node, final int code, final int depth){
        //fixme kadidát na iterativní přepis
        if (node.isLeaf()){
            if (this.maxDepth < depth){
                this.maxDepth = depth;
            }
            this.huffmanCache[node.getItem()] = new Code(code, depth);
            return;
        }

        this.buildCache(node.getLeftChild(), code << 1 | 1, depth + 1);
        this.buildCache(node.getRightChild(), code << 1, depth + 1);
    }

    /**
     * Builds header which is byte array containing compressed
     * HuffmanTree. This header should be placed at the beginning
     * of encoded file. First 3 bits are zeros - those should be
     * replaced with count of trailing bit at the end of compressed
     * file so decoder know where to stop decoding. {@link HuffmanTree#getHeader()}
     */
    private void buildHeader() {
        if (this.head == null){
            return;
        }

        try (final ByteArrayOutputStream out = new ByteArrayOutputStream();
             final DataOutputStream header = new DataOutputStream(out)){

            final Deque<HuffmanNode> stack = new ArrayDeque<>();
            int buffer = 0;
            int index = 3;
            int temp;
            int complement;

            stack.push(this.head);

            while (!stack.isEmpty()) {
                final HuffmanNode node = stack.pop();

                // check buffer overflow
                if (index >= Byte.SIZE) {
                    header.writeByte(buffer);
                    buffer = 0;
                    index = 0;
                }

                // push node marker
                buffer <<= 1;
                buffer |= node.isLeaf() ? 1 : 0;
                ++index;

                if (!node.isLeaf()) {
                    stack.push(node.getRightChild());
                    stack.push(node.getLeftChild());
                    continue;
                }

                // fill the buffer send it and push the rest to new buffer
                temp = node.getItem();
                complement = Byte.SIZE - index;
                buffer <<= complement;
                buffer |= temp >> index;
                header.writeByte(buffer);
                buffer <<= index;
                buffer |= (temp << complement) >> complement;
            }

            if (index > 0) {
                complement = Byte.SIZE - index;
                buffer <<= complement;
                header.write(buffer);
                this.headerOffset = (byte) complement;
            }

            this.header = out.toByteArray();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Traverse the tree until code of specified length
     * is reached. If there is no such leaf on the path
     * then null is returned.
     *
     * @param path potential path
     * @param length length of the path
     * @return leaf value or null if leaf is not found
     * @deprecated use {@link HuffmanTree#traverseUntil(int, int)}
     */
    public Integer traverseTo(final int path, final int length){
        HuffmanNode node = this.head;
        for (int i = length - 1; i >= 0; --i){
            node = (((path >>> i) & 1) == 1)
                    ? node.getLeftChild()
                    : node.getRightChild();
        }
        if (node == null || !node.isLeaf()){
            return null;
        }
        return node.getItem();
    }

    /**
     * Traverses the tree until leaf is reached. Input is
     * potential path to leaf which has to be superset of
     * the actual path and length of the potential path.
     * <br><br>
     * The path has to be aligned right and is parsed in
     * left-to-right manner.
     * <br><br>
     * If path is too short to contain any valid code
     * then returned length is equal to the initial path
     * length passed as parameter and if you build this tree
     * by passing occurrence map to the constructor or used
     * the HuffmanTreeTopDownBuilder.toProperTree() method
     * then it's safe to assume that returned item is -1.
     *
     * @param path potential path
     * @param length length of valid sequence
     * @return array consisting of decoded item and its code length
     */
    Code traverseUntil(final int path, int length) {
        HuffmanNode node = this.head;
        int i;
        for(i = --length; i >= 0 && !node.isLeaf(); --i){
            node = (((path >>> i) & 1) == 1)
                    ? node.getLeftChild()
                    : node.getRightChild();
        }

        return new Code(node.getItem(), length - i);
    }

    /**
     * Returns maximal depth of the tree.
     *
     * @return maximal depth.
     */
    int getMaxDepth(){
        return this.maxDepth;
    }
}
