package cz.zcu.fav.kiv.ti.huffman.codec;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;

/**
 * HuffmanTreeTopDownBuilder class
 * <br><br>
 * Preferred way to build a tree in top-down manner,
 * e.g. while reconstructing it from encoded file, is
 * to use this builder.
 *
 * @since 5.11.2018
 * @author Martin Procházka
 */
class HuffmanTreeTopDownBuilder {
    /**
     * Stack for keeping reference to unfinished nodes
     */
    private final Deque<MutableNode> stack;

    /**
     * Head of the tree
     */
    private MutableNode head;

    /**
     * Class constructor, nothing special about this one.
     */
    HuffmanTreeTopDownBuilder(){
        this.stack = new ArrayDeque<>();
    }

    /**
     * Node is created with passed value and added to the tree.
     * You need to add nodes in preorder manner for this to work
     * properly.
     * <br><br>
     * The builder can tell when the tree is complete and it won't
     * append more nodes upon calling this and will throw exception.
     * <br><br>
     * Use {@link HuffmanTreeTopDownBuilder#isComplete()} to see if tree is complete.
     *
     * @param value node value
     * @throws Exception thrown if tree is complete
     */
    void addNode(final int value) throws Exception {
        final MutableNode node = new MutableNode(value);

        if (this.isComplete()){
            throw new Exception("Tree is complete");
        } else if(this.stack.isEmpty()){
            this.head = node;
            if (this.head.isLeaf){
                return;
            }
            this.stack.push(node);
            return;
        }

        MutableNode parent = Objects.requireNonNull(this.stack.peek());

        if (parent.leftChild == null) {
            parent.leftChild = node;
        } else if(parent.rightChild == null){
            parent.rightChild = node;
        }

        if (!node.isLeaf) {
            this.stack.push(node);
        } else {
            while (parent.leftChild != null && parent.rightChild != null){
                this.stack.pop();
                if (this.stack.isEmpty()){
                    return;
                }
                parent = Objects.requireNonNull(this.stack.peek());
            }
        }
    }

    /**
     * Return true if tree is complete and false if it is not.
     * The fact that tree is complete in this case means that
     * there is no non-leaf node with unset left or right child.
     *
     * @return true if tree is complete false otherwise
     */
    boolean isComplete(){
        return this.head != null && this.stack.isEmpty();
    }

    /**
     * Build HuffmanTree and return reference to its instance.
     *
     * @return reference to HuffmanTree
     */
    HuffmanTree toProperTree(){
        return new HuffmanTree(this.head.toHuffmanNode());
    }


    /**
     * MutableNode private class
     * <br><br>
     * This is simplified version of HuffmanNode that is also
     * mutable so it is possible to come back to already existing
     * node and fill in its children.
     *
     * @since 5.11.2018
     * @author Martin Procházka
     */
    private class MutableNode {
        /**
         * Value of node
         */
        final int value;

        /**
         * Reference to left child
         */
        MutableNode leftChild;

        /**
         * Reference to right child
         */
        MutableNode rightChild;

        /**
         * This is true if node is leaf false otherwise
         */
        final boolean isLeaf;

        /**
         * Class constructor
         * <br><br>
         * Only initializes value of this node to the one passed
         * as argument and initializes "leafness" of this node -
         * if value is positive integer or zero then node is leaf
         * otherwise it is treated as non-leaf.
         *
         * @param value desired value
         */
        MutableNode(final int value){
            this.value = value;
            this.isLeaf = value >= 0;
        }

        /**
         * This recursively builds subtree with this node as
         * its head and returns this node. All nodes in this
         * subtree are immutable HuffmanNodes.
         *
         * @return this node as instance of HuffmanNode
         */
        HuffmanNode toHuffmanNode(){
            return (this.isLeaf)
                    ? new HuffmanNode(this.value, 1)
                    : new HuffmanNode(this.leftChild.toHuffmanNode(), this.rightChild.toHuffmanNode());
        }
    }
}
