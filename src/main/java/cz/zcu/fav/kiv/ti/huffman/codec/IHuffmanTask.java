package cz.zcu.fav.kiv.ti.huffman.codec;

import java.io.IOException;

/**
 * IHuffmanTask interface
 *
 * This interface unifies access to encoder
 * and decoder from outside of the package.
 *
 * @author Martin Procházka
 * @since 14.11.2018
 */
public interface IHuffmanTask {
    /**
     * Executes the task.
     * Progress will be tracked when using this.
     *
     * @param target path to target file
     * @throws Exception see the implementations
     */
    void doTaskVerbosely(String target) throws Exception;

    /**
     * Executes the task.
     *
     * @param target path to target file
     * @throws Exception see the implementations
     */
    void doTask(String target) throws Exception;

    /**
     * Prints information gathered before executing
     * the task to stdin. If more is true then data
     * structures needed for the task are printed too.
     *
     * @param more print data structures
     */
    void echoPre(boolean more);

    /**
     * Prints information gathered during and after
     * executing the task to stdin. If more is true
     * then data structures needed for the task are
     * printed too.
     * <br><br>
     * This will probably not work as you expect if
     * task was not yet executed.
     *
     * @param more print data structures
     */
    void echoPost(boolean more) throws IOException;
}
