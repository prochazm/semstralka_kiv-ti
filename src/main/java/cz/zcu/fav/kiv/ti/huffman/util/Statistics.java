package cz.zcu.fav.kiv.ti.huffman.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Lukáš Kvídera
 */
public class Statistics implements Iterable<Statistics.Occurrence> {

    private static final int ARRAY_SIZE = 256;
    private final Counter[] statistics = new Counter[ARRAY_SIZE];

    public Statistics() {
        for (int i = 0; i < this.statistics.length; i++) {
            this.statistics[i] = new Counter();
        }
    }

    public void increment(final int c) {
        this.statistics[c].increment();
    }

    @Override
    public Iterator<Occurrence> iterator() {
        return new IteratorImpl();
    }

    public Integer get(final int a) {
        return this.statistics[a].count;
    }

    public int size() {
        int size = 0;
        for (final Occurrence c : this) {
            ++size;
        }

        return size;
    }

    public static class Occurrence {

        private final Integer key;
        private final Integer value;

        public Occurrence(final Integer key, final Integer value) {
            this.key = key;
            this.value = value;
        }

        public Integer getKey() {
            return this.key;
        }

        public Integer getValue() {
            return this.value;
        }
    }

    private static class Counter {
        private int count;

        void increment() {
            ++this.count;
        }

        int getCount() {
            return this.count;
        }
    }

    private class IteratorImpl implements Iterator<Occurrence> {
        private int position = -1;
        private int nextPosition;

        @Override
        public boolean hasNext() {
            this.nextPosition = findNext();
            return isNotAtTheEnd(this.nextPosition);
        }

        private boolean isNotAtTheEnd(final int nextPosition) {
            return nextPosition < ARRAY_SIZE;
        }

        @Override
        public Occurrence next() {
            if (advance()) {
                return new Occurrence(this.position, Statistics.this.statistics[this.position].getCount());
            }

            throw new NoSuchElementException();
        }

        private boolean advance() {
            if (isNotAtTheEnd(this.nextPosition)) {
                this.position = this.nextPosition;
                return true;
            }
            return false;
        }

        private int findNext() {
            int nextPossiblePosition = this.position;
            do {
                ++nextPossiblePosition;
            }
            while (isNotAtTheEnd(nextPossiblePosition) && Statistics.this.statistics[nextPossiblePosition].count == 0);

            return nextPossiblePosition;
        }
    }
}