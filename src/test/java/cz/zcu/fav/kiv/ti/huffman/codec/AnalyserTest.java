package cz.zcu.fav.kiv.ti.huffman.codec;

import cz.zcu.fav.kiv.ti.huffman.util.Statistics;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class AnalyserTest {

    private File temp;
    private Analyser analyser;

    @Before
    public void setUp() throws Exception {
        this.temp = File.createTempFile("AnalyserTest",".tmp");
        this.analyser = new Analyser(this.temp.getAbsolutePath());
    }

    @After
    public void tearDown(){
        this.temp.deleteOnExit();
    }

    private void prepNullFile() throws Exception{
        try (final BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(this.temp.getAbsolutePath()))){
            os.write(new byte[]{
                    (byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,
                    (byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,
                    (byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,
                    (byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,
            });
        }
    }

    private void prepSingleCharFile() throws Exception{
        try (final BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(this.temp.getAbsolutePath()))){
            os.write(new byte[]{
                    (byte)0x61,(byte)0x61,(byte)0x61,(byte)0x61,
                    (byte)0x61,(byte)0x61,(byte)0x61,(byte)0x61,
                    (byte)0x61,(byte)0x61,(byte)0x61,(byte)0x61,
                    (byte)0x61,(byte)0x61,(byte)0x61,(byte)0x61,
            });
        }
    }

    private void prepMultiCharFile() throws Exception{
        try (final BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(this.temp.getAbsolutePath()))){
            os.write(new byte[]{
                    (byte)0x61,(byte)0x61,(byte)0x61,(byte)0x62,
                    (byte)0x61,(byte)0x64,(byte)0x63,(byte)0x62,
                    (byte)0x61,(byte)0x62,(byte)0x62,(byte)0x63,
                    (byte)0x61,(byte)0x61,(byte)0x62,(byte)0x61,
            });
        }
    }

    @Test
    public void getOccurrencesMultiChar_properOccurrenceMap() throws Exception {
        this.prepMultiCharFile();
        final Statistics om = this.analyser.getOccurrences(false);

        assertEquals("Invalid count of a", Integer.valueOf(8), om.get((int) 'a'));
        assertEquals("Invalid count of b", Integer.valueOf(5), om.get((int) 'b'));
        assertEquals("Invalid count of c", Integer.valueOf(2), om.get((int) 'c'));
        assertEquals("Invalid count of d", Integer.valueOf(1), om.get((int) 'd'));
        assertEquals("Invalid occurrences present", 4, om.size());
    }

    @Test
    public void getOccurrencesSingleChar_OccurrenceMapWithPlaceholder() throws Exception {
        this.prepSingleCharFile();
        final Statistics om = this.analyser.getOccurrences(false);

        assertEquals("Invalid count of a", Integer.valueOf(16), om.get((int) 'a'));
        assertEquals("Redundant occurrences are present", 1, om.size());
    }

    @Test
    public void getOccurrencesNullFile_OccurrenceMapSingleItem() throws Exception {
        this.prepNullFile();
        final Statistics om = this.analyser.getOccurrences(false);

        assertEquals("Invalid count of \\0", Integer.valueOf(16), om.get(0x00));
        assertEquals("Redundant occurrences are present", 1, om.size());
    }

    @Test
    public void compressionRatioSingleCharFile_correctRatio() throws Exception {
        this.prepSingleCharFile();

        assertEquals("Invalid compress ratio", 5.333, this.analyser.getCompressionRatio(), .001);
    }

    @Test
    public void compressionRatioMultiCharFile_correctRatio() throws Exception {
        this.prepMultiCharFile();

        assertEquals("Invalid compress ratio", 2, this.analyser.getCompressionRatio(), .001);
    }
}
