package cz.zcu.fav.kiv.ti.huffman.codec;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.*;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class DecoderTest {

    private File tempIn;
    private File tempOut;
    private IHuffmanTask decoder;

    @Before
    public void setUp() throws Exception {
        this.tempIn = File.createTempFile("decoderTestIn",".tmp");
        this.tempOut = File.createTempFile("decoderTestOut",".tmp");
        this.decoder = Decoder.getDecoder(this.tempIn.getAbsolutePath());
    }

    @After
    public void tearDown(){
        this.tempIn.deleteOnExit();
        this.tempOut.deleteOnExit();
    }

    @Test(expected = Exception.class)
    public void emptyFile_emptyFile() throws Exception {
        this.decoder.doTask(this.tempOut.getAbsolutePath());

        try (final BufferedInputStream is = new BufferedInputStream(new FileInputStream(this.tempOut))){
            assertEquals("File is not empty", -1, is.read());
        }
    }

    @Test
    public void simpleFile_correctString() throws Exception {
        try (final BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(this.tempIn))){
            os.write(new byte[]{
                        (byte)0x8B, (byte)0x0A, (byte)0xC4,
                        (byte)0xB1, (byte)0xD9, (byte)0x3B,
                        (byte)0x05, (byte)0xA9, (byte)0xD0,
            });
        }
        this.decoder.doTask(this.tempOut.getAbsolutePath());

        try (final BufferedInputStream is = new BufferedInputStream(new FileInputStream(this.tempOut))){
            final byte[] contents = new byte[15];
            is.read(contents);

            assertEquals("File is not empty", -1, is.read());
            assertArrayEquals("Bad output", "aaabadcbabbcaab".getBytes(), contents);
        }
    }
}
