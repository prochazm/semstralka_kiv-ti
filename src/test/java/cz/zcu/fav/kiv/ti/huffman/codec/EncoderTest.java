package cz.zcu.fav.kiv.ti.huffman.codec;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.*;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class EncoderTest {

    private File tempIn;
    private File tempOut;
    private IHuffmanTask encoder;

    @Before
    public void setUp() throws Exception {
        this.tempIn = File.createTempFile("encoderTestIn",".tmp");
        this.tempOut = File.createTempFile("encoderTestOut",".tmp");
        this.encoder = Encoder.getEncoder(this.tempIn.getAbsolutePath());
    }

    @After
    public void tearDown(){
        this.tempIn.deleteOnExit();
        this.tempOut.deleteOnExit();
    }

    @Test(expected = IOException.class)
    public void encodeEmptyFile_emptyFile() throws Exception {
        this.encoder.doTask(this.tempOut.getAbsolutePath());
    }

    @Test
    public void encodeSameCharFile_encodedFile() throws Exception{
        try (final BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(this.tempIn))){
            os.write("aaaaaaaa".getBytes());
        }
        this.encoder.doTask(this.tempOut.getAbsolutePath());
        try (final BufferedInputStream is = new BufferedInputStream(new FileInputStream(this.tempOut))){
            assertEquals("Bad byte #1", 0x96, is.read());
            assertEquals("Bad byte #2", 0x1F, is.read());
            assertEquals("Bad byte #3", 0xF0, is.read());
            assertEquals("Input should end here", -1, is.read());
        }
    }

    @Test
    public void encodeMultiCharFile_encodedFile() throws Exception{
        try (final BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(this.tempIn))){
            os.write("ababcad".getBytes());
        }
        this.encoder.doTask(this.tempOut.getAbsolutePath());
        try (final BufferedInputStream is = new BufferedInputStream(new FileInputStream(this.tempOut))){
            assertEquals("Bad byte #1", 0x2B, is.read());
            assertEquals("Bad byte #2", 0x0A, is.read());
            assertEquals("Bad byte #3", 0xC4, is.read());
            assertEquals("Bad byte #4", 0xB1, is.read());
            assertEquals("Bad byte #5", 0xD9, is.read());
            assertEquals("Bad byte #6", 0x2D, is.read());
            assertEquals("Bad byte #7", 0x30, is.read());
            assertEquals("Input should end here", -1, is.read());
        }
    }
}
