package cz.zcu.fav.kiv.ti.huffman.codec;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class HuffmanNodeTest {

    private final HuffmanNode genericNode1 = new HuffmanNode(1,1);
    private final HuffmanNode genericNode2 = new HuffmanNode(2,2);

    @Test
    public void isNonLeafALeaf_false(){
        final HuffmanNode node = new HuffmanNode(this.genericNode1, this.genericNode2);
        assertFalse("Non-leaf node is leaf", node.isLeaf());
    }

    @Test
    public void isLeafALeaf_True(){
        assertTrue("Leaf node is a non-leaf", this.genericNode1.isLeaf());
        assertTrue("Leaf node is a non-leaf", this.genericNode2.isLeaf());
    }


}
