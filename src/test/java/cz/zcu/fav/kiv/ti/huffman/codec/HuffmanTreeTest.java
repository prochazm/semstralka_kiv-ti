package cz.zcu.fav.kiv.ti.huffman.codec;

import cz.zcu.fav.kiv.ti.huffman.util.Statistics;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Objects;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class HuffmanTreeTest {

    private void fillUntil(final Statistics statistics, final int character, final int occurrence){
        for (int i = 0; i < occurrence; ++i){
            statistics.increment(character);
        }
    }

    private HuffmanTree getTreeTwoCharacters() throws Exception {
        final Statistics statistics = new Statistics();
        this.fillUntil(statistics, 'a', 10);
        this.fillUntil(statistics, 'b', 5);

        return new HuffmanTree(statistics);
    }

    private HuffmanTree getTreeThreeCharacters() throws Exception {
        final Statistics statistics = new Statistics();
        this.fillUntil(statistics, 'a', 10);
        this.fillUntil(statistics, 'b', 5);
        this.fillUntil(statistics, 'c', 1);

        return new HuffmanTree(statistics);
    }

    private HuffmanTree getTreeSixCharacters() throws Exception {
        final Statistics statistics = new Statistics();
        this.fillUntil(statistics, 'a', 10);
        this.fillUntil(statistics, 'b', 8);
        this.fillUntil(statistics, 'c', 7);
        this.fillUntil(statistics, 'd', 5);
        this.fillUntil(statistics, 'e', 10);
        this.fillUntil(statistics, 'f', 7);

        return new HuffmanTree(statistics);
    }

    @Test
    public void getHeader_ProperHeaderNoOffset() throws Exception {
        final HuffmanTree tree = this.getTreeThreeCharacters();
        final byte[] header = tree.getHeader();

        assertEquals("First byte is wrong",  (byte)0x05, header[0]);
        assertEquals("Second byte is wrong", (byte)0x8E, header[1]);
        assertEquals("Third byte is wrong",  (byte)0xC5, header[2]);
        assertEquals("Fourth byte is wrong", (byte)0x61, header[3]);
        assertEquals("There should be no offset", 0, tree.getHeaderOffset());
    }

    @Test
    public void getHeader_ProperHeaderWithOffset() throws Exception {
        final HuffmanTree tree = this.getTreeTwoCharacters();
        final byte[] header = tree.getHeader();

        assertEquals("first byte is wrong",  (byte)0x0B, header[0]);
        assertEquals("second byte is wrong", (byte)0x15, header[1]);
        assertEquals("Third byte is wrong",  (byte)0x84, header[2]);
        assertEquals("There should be no offset", 2, tree.getHeaderOffset());
    }


    @Test
    public void getHead_ProperStructure() throws Exception {
        final HuffmanTree tree = this.getTreeTwoCharacters();
        final HuffmanNode head = tree.getHead();

        assertEquals("Head has has wrong count", 15, head.getCount());
        assertEquals("Left child has wrong item", (int)'b', head.getLeftChild().getItem());
        assertEquals("Right child has wrong item", (int)'a', head.getRightChild().getItem());
        assertFalse("Head is leaf", head.isLeaf());
        assertTrue("Left child is not leaf", head.getLeftChild().isLeaf());
        assertTrue("Right child is not leaf", head.getRightChild().isLeaf());
    }

    @Test
    public void hasItem_TrueIfItemPresent() throws Exception {
        final HuffmanTree tree = this.getTreeSixCharacters();

        for (int i = 'a'; i <= 'f'; ++i){
            assertTrue("Item not found", tree.hasItem(i));
        }
        for (int i = 'g'; i <= 'z'; ++i) {
            assertFalse("Item found", tree.hasItem(i));
        }
    }

    @Test
    public void getItem_itemCode() throws Exception{
        final HuffmanTree tree = this.getTreeSixCharacters();

        assertEquals("Wrong code", 0b11,tree.getItem('a').getCode());
        assertEquals("Wrong code length", 2,tree.getItem('a').getLength());
        assertEquals("Wrong code", 0b000, tree.getItem('b').getCode());
        assertEquals("Wrong code length", 3, tree.getItem('b').getLength());
    }

    @Test
    public void getItems_allItemCodes() throws Exception{
        final HuffmanTree tree = this.getTreeSixCharacters();

        final Code[] items = tree.getItems();

        assertEquals("Bad code for a", new Code(0b11 , 2), items[(int)'a']);
        assertEquals("Bad code for b", new Code(0b000, 3), items[(int)'b']);
        assertEquals("Bad code for c", new Code(0b010, 3), items[(int)'c']);
        assertEquals("Bad code for d", new Code(0b011, 3), items[(int)'d']);
        assertEquals("Bad code for e", new Code(0b10 , 2), items[(int)'e']);
        assertEquals("Bad code for f", new Code(0b001, 3), items[(int)'f']);
        assertEquals("Redundant code(s) present", 6, Arrays.stream(items).filter(Objects::nonNull).count());
    }

    @Test
    public void getDepthDeepTree_Three() throws Exception{
        assertEquals("Wrong depth", 3, this.getTreeSixCharacters().getMaxDepth());
    }


    @Test
    public void getDepthDeepTree_One() throws Exception{
        assertEquals("Wrong depth", 1, this.getTreeTwoCharacters().getMaxDepth());
    }

    @Test
    public void traverseUntilMultipleTraversals_CorrectItems() throws Exception{
        final HuffmanTree tree = this.getTreeSixCharacters();

        final Code c = tree.traverseUntil(0b0101111, 7);
        assertEquals("Invalid item or code length", new Code((int)'c', 3), c);
        final Code a1 = tree.traverseUntil(0b1111, 4);
        assertEquals("Invalid item or code length", new Code((int)'a', 2), a1);
        final Code a2 = tree.traverseUntil(0b11, 2);
        assertEquals("Invalid item or code length", new Code((int)'a', 2), a2);
        final Code x = tree.traverseUntil(0b0, 2);
        assertEquals("Invalid item or code length", new Code(-1, 2), x);
    }
}
