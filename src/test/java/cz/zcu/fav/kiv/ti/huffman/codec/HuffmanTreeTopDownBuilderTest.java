package cz.zcu.fav.kiv.ti.huffman.codec;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HuffmanTreeTopDownBuilderTest {

    private HuffmanTreeTopDownBuilder builder;

    @Before
    public void setUp(){
        this.builder = new HuffmanTreeTopDownBuilder();
    }

    @Test
    public void addSingleNode_SingleNode() throws Exception {
        this.builder.addNode(1);
        final HuffmanTree tree = this.builder.toProperTree();

        assertTrue("Tree is incomplete", this.builder.isComplete());
        assertNotNull("Tree is null", tree);
    }

    @Test
    public void addThreeNodes_CompleteTree() throws Exception {
        this.builder.addNode(-1);
        this.builder.addNode(1);
        this.builder.addNode(2);

        final HuffmanTree tree = this.builder.toProperTree();

        assertNotNull("Tree is null", tree);
        assertFalse("Head is leaf", tree.getHead().isLeaf());
        assertTrue("Left child is not leaf", tree.getHead().getLeftChild().isLeaf());
        assertTrue("Right child is not leaf", tree.getHead().getRightChild().isLeaf());
        assertEquals("Bad value", -1, tree.getHead().getItem());
        assertEquals("Bad value", 1, tree.getHead().getLeftChild().getItem());
        assertEquals("Bad value", 2, tree.getHead().getRightChild().getItem());
    }

    @Test(expected = NullPointerException.class)
    public void addTwoNodes_IncompleteTreeThrowsNPE() throws Exception{
        this.builder.addNode(-1);
        this.builder.addNode(1);
        this.builder.toProperTree();
    }

    @Test
    public void isCompleteSingleNodeNotLeaf_Incomplete() throws Exception {
        this.builder.addNode(-1);

        assertFalse("Tree is complete", this.builder.isComplete());
    }

    @Test
    public void isCompleteTwoNodes_Incomplete() throws Exception {
        this.builder.addNode(-1);
        this.builder.addNode(1);

        assertFalse("Tree is complete", this.builder.isComplete());
    }

    @Test
    public void isCompleteThreeNodes_Complete() throws Exception {
        this.builder.addNode(-1);
        this.builder.addNode(1);
        this.builder.addNode(2);

        assertTrue("Tree is incomplete", this.builder.isComplete());
    }
}
